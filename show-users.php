<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Matches</title>
<link rel="stylesheet"
  type="text/css"
  href="style.css"
  />

</head>
<body><div id="main">

<!-- CONTENT HERE -->

  <h1>All Users</h1>
  <?php
  $mysqli = new mysqli('localhost', 'tinder', 'tinder', 'matchmaker');

    if($mysqli->connect_errno) {
      printf("Connection Failed: %s\n", $mysqli->connect_error);
      exit;
    }
    $stmt = $mysqli->prepare('select name, email, age, description, pictureUrl from users');
    if(!$stmt){
      printf("Query Prep Failed: %s\n", $mysqli->error);
      exit;
    }
    $stmt->execute();
    $stmt->bind_result($name, $email, $age, $description, $pictureUrl);
    while($stmt->fetch()){
      echo "<div>";
        echo htmlentities($name);
        echo htmlentities($email);
        echo htmlentities($age);
        echo htmlentities($description);
        echo "<img src='".htmlentities($pictureUrl)."'/>";
      echo "</div>";
    }


   ?>

   <form action='age-range.php' method='GET'>
     <label>low</label><input type='number' name='low' />
     <label>high</label><input type='number' name='high' />
     <input type='submit' />
   </form>
</div></body>
</html>
