<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Matchmaking site - Users by age</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">

<!-- CONTENT HERE -->
<?php
  $low = (int)$_GET['low'];
  $high = (int)$_GET['high'];

  $mysqli = new mysqli('localhost', 'tinder', 'tinder', 'matchmaker');

  if($mysqli->connect_errno) {
    printf("Connection Failed: %s\n", $mysqli->connect_error);
    exit;
  }

  $stmt = $mysqli->prepare('select name, email, age, description from useres where (user.age) < (?) and (user.age) > (?) ');
  $stmt->bind_param('ii', $low, $high);
  $stmt->execute();
  $stmt->bind_result($name, $email, $age, $description, $pictureUrl);
  while($stmt->fetch()){
    echo "<div>";
      echo htmlentities($name);
      echo htmlentities($email);
      echo htmlentities($age);
      echo htmlentities($description);
      echo "<img src='".htmlentities($pictureUrl)."'/>";
    echo "</div>";
  }
 ?>

 <a href='show-users.php'>Go bacak</a>

</div></body>
</html>
