CREATE DATABASE matchmaker;
CREATE user 'tinder'@'localhost' identified BY 'tinder';
GRANT SELECT,INSERT,UPDATE,DELETE on matchmaker.* to tinder@'localhost';
use matchmaker;
CREATE TABLE users (
  id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50),
  email VARCHAR(50),
  UNIQUE key unique_email (email),
  pictureUrl VARCHAR(255),
  description TINYTEXT,
  age TINYINT UNSIGNED NOT NULL,
  posted TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);
